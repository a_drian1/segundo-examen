/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public interface Color {
    public void pintarColor();
}

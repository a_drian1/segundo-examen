/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Circulo extends Figura{
    
    public Circulo(){
        this.color = new Transparente();
    }

    @Override
    public void dibujarFigura(){
        System.out.print("Dibujando circulo. ");
        color.pintarColor();
    }

    @Override
    public void elejirColor(Color color) {
        this.color = color;
    }
    
}

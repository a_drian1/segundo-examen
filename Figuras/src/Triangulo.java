/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Triangulo extends Figura{
    
    public Triangulo(){
        this.color = new Transparente();
    }
    
    @Override
    public void dibujarFigura() {
        System.out.print("Dibujando triangulo. ");
        color.pintarColor();
    }

    @Override
    public void elejirColor(Color color) {
        this.color = color;
    }
    
}

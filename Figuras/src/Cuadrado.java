/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Cuadrado extends Figura{
    
    public Cuadrado(){
        this.color = new Transparente();
    }

    @Override
    public void dibujarFigura() {
        System.out.print("Dibujando cuadrado. ");
        color.pintarColor();
    }
    
    @Override
    public void elejirColor(Color color){
        this.color = color;
    }
    
}

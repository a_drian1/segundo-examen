/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Rojo implements Color{
    @Override
    public void pintarColor() {
        System.out.println("Pintando de color Rojo.");
    }    
}

/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Principal {
    public static void main(String [] args){
        //Color como intefaz y asignacion de una implementacion de color.
        Color rojo = new Rojo();
        Color azul = new Azul();
        
        //Figura como clase abstracta y asignacion de subclase de figura.
        Figura circulo = new Circulo();
        Figura cuadrado = new Cuadrado();               
        
        //Al dibujar la figura en un principio es transparente.        
        circulo.dibujarFigura();  
        
        //Elejimos una implementacion de color para pintar el cuadrado y ciruclo.
        cuadrado.elejirColor(azul);
        cuadrado.dibujarFigura();      
        circulo.elejirColor(rojo);
        circulo.dibujarFigura();
    }
}

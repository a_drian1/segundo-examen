/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Transparente implements Color{
    @Override
    public void pintarColor() {
        System.out.println("Dejando el color transparente");
    }    
}

/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public abstract class Figura {
    //Toda subclase de figura debe de tener una implementacion de la interfaz color.
    Color color;
    
    public abstract void dibujarFigura();
    public abstract void elejirColor(Color color);
    
}

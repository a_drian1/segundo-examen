Alumno: Adrián Ibarra González
Matricula: 37180797
Patrón de Diseño Implementado: Bridge.

Descripcion:
El problema es el crecimiento acelerado de las clases con función de
(Clases = abstracciones*implementaciones) las cuales son las combinaciones cada
clase. En el patron bridge se utilizan (Clases = abstracciones + implementaciones).
Además de que cuidamos que el cliente solo se preocupe por llamar a la abstracción
y no en los detalles de implementación, ya que se encuentran separados.

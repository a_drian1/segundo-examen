/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class PizzaPeperoni extends RecetaBuilder{

    @Override
    public void mezclarMasa() {
        String amasado = "";
        amasado +="\n-Añadir 500 gramos de Harina de trigo.";
        amasado +="\n-Añadir un sobre de levadura.";
        amasado +="\n-Añadir una cucharada de aceite.";
        amasado +="\n-Mezclar.";
        amasado +="\n-Amasar...";
        
        receta.amasado(amasado);
    }

    @Override
    public void cocinarMasa() {
        receta.tipoCocinado("\n-Cocinar en horno moderado 15 minutos.");
        
    }

    @Override
    public void añadirIngredientesExtra() {
        String ingredientes = "";
        ingredientes +="\n-Añadir pure de tomate.";        
        ingredientes +="\n-Añadir Salchicha.";
        ingredientes +="\n-Añadir Queso.";
        ingredientes +="\n-Añadir jamón.";
        ingredientes +="\n-Añadir Extra Peperoni.";
        receta.ingredientesAdicionales(ingredientes);
    }
    
}

/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public abstract class RecetaBuilder {
    
    protected Receta receta;
    
    public Receta obtenerReceta(){
        return receta;
    }
    
    public void crearNuevaReceta(){
        receta = new Receta();
    }
    
    public abstract void mezclarMasa();
    public abstract void cocinarMasa();
    public abstract void añadirIngredientesExtra();
}

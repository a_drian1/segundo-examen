/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Recetario {
    private RecetaBuilder recetaBuilder;
    
    public void setRecetaBuilder(RecetaBuilder rb){
        recetaBuilder = rb;
    }
    
    public Receta obtenerReceta(){
        return recetaBuilder.obtenerReceta();
    }
    
    public void construirReceta(){  
        recetaBuilder.crearNuevaReceta();
        recetaBuilder.mezclarMasa();
        recetaBuilder.cocinarMasa();
        recetaBuilder.añadirIngredientesExtra();        
    }
}

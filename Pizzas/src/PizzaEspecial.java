/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class PizzaEspecial extends RecetaBuilder{

    @Override
    public void mezclarMasa() {
        String amasado = "";
        amasado +="\n-Añadir 400 gramos de harina de trigo.";
        amasado +="\n-Añadir 1 cucharada de levadura.";
        amasado +="\n-Añadir 1 cucharada de aceite aceite.";
        amasado +="\n-Añadir 1 huevo.";
        amasado +="\n-Mezclar.";
        amasado +="\n-Amasar...";        
        receta.amasado(amasado);
    }

    @Override
    public void cocinarMasa() {        
        receta.tipoCocinado("\n-Cocinar fuego moderado-alto 10 minutos...");
    }

    @Override
    public void añadirIngredientesExtra() {       
        String ingredientes = "";
        ingredientes+="\n-Añadir chorizo.";
        ingredientes+="\n-Añadir jalapeños.";
        ingredientes+="\n-Añadir extra picante.";
        ingredientes+="\n-Añadir salsa inglesa.";
        ingredientes+="\n-Añadir queso...";        
        receta.ingredientesAdicionales(ingredientes);
    }
    
}

/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Principal {
    public static void main(String[] args){
        Recetario recetario = new Recetario();
        PizzaEspecial pizzaEspecial = new PizzaEspecial();
        PizzaPeperoni pizzaPeperoni = new PizzaPeperoni();
        Pastel pastel = new Pastel();
        
        recetario.setRecetaBuilder(pastel);
        recetario.construirReceta();
        
        Receta receta = recetario.obtenerReceta();
        
        receta.imprimirReceta();
        
    }    
}

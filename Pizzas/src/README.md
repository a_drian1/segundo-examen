Alumno: Adrián Ibarra González
Matricula: 37180797
Patrón de Diseño Implementado: Builder.

Descripcion:
Implementé este patrón de diseño porque estamos tratando crear diferentes representaciones
de la clase receta y para que la receta cambiara de forma muy fácil y con cada 
una de los módulos con una sola responsabilidad. Además de que lo tube que adaptar para
que cada uno de los concrete builders pudieran tener los mismos pasos de construccion.

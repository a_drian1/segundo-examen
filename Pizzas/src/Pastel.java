/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Pastel extends RecetaBuilder{

    @Override
    public void mezclarMasa() {
        String amasado = "\n-Añadir 3 tazas de Harina.";
        amasado += "\n-Añadir 2 cucharadas de levadura.";
        amasado += "\n-Añadir 3 yemas de huevo.";
        amasado += "\n-Añadir 3 cucharadas de leche.";
        amasado += "\n-Mezclar.";
        amasado += "\n-Amasar...";

        receta.amasado(amasado);
    }

    @Override
    public void cocinarMasa() {        
        receta.tipoCocinado("\n-Cocinado 30 minutos.");        
    }

    @Override
    public void añadirIngredientesExtra() {
        String ingredientes = "";
        ingredientes += "\n-Añadir betun";
        ingredientes +="\n-Añadir adornos de decoracion.";
        ingredientes +="\n-Añadir la cereza en el pastel.";
        receta.ingredientesAdicionales(ingredientes);
    }
    
}

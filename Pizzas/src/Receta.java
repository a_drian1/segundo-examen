/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Receta {   
    public String amasado;
    public String tipoCocinado;
    public String ingredientesAdicionales;
    
    public void amasado(String amasado){
        this.amasado = amasado;
    }
    
    public void tipoCocinado(String cocinado){
        this.tipoCocinado = cocinado;
    }
    
    public void ingredientesAdicionales(String ingredientes){
        this.ingredientesAdicionales = ingredientes;
    }
    
    public void imprimirReceta(){
        System.out.println("Receta:");
        System.out.println(amasado);
        System.out.println(tipoCocinado);
        System.out.println(ingredientesAdicionales);
    }
    
}

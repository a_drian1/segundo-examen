Alumno: Adrián Ibarra González.
Matricula: 37180797
Patrón de Diseño Implementado: Observador.

Descirpción:
En el problema menciona que necesita, notificar a una lista de suscriptores con
un email, esto lo conseguimos con el patron observer, en el ejemplo se ve cómo 
cada una de los suscriptores hace una cosa diferente con la notificacion.
Además el patrón nos da la posibilidad agregar cualquier cantidad de suscriptores.

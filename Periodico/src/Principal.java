/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Principal {
    public static void main(String[] args){
        //Creando la revista de interfaz publisher
        Publisher revista = new Revista();
        //Creando los suscriptores de clase abstracta suscriptor.
        //Hice la modificacion de utilizar clase abstracta para que pudiera guardar el nombre
        //del suscriptor en vez de utilizar una interfaz.
        Suscriptor suscriptor1 = new Suscriptor1("Antonio Perez");
        Suscriptor suscriptor2 = new Suscriptor2("Juan Perez");
        Suscriptor suscriptor3 = new Suscriptor3("Jorge Hernandez");
        
        revista.agregarSuscriptor(suscriptor1);
        revista.agregarSuscriptor(suscriptor2);
        revista.agregarSuscriptor(suscriptor3);
        //Notificamos a todos los suscriptores.
        revista.notificar();            
    
    }               
}

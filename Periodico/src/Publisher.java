/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public interface Publisher {    
    public void notificar();
    public void agregarSuscriptor(Suscriptor suscriptor);
    public void eliminarSuscriptor(Suscriptor suscriptor);
}

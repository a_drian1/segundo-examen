/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public abstract class Suscriptor {
    public String nombreDeIdentificacion;
    
    public abstract void actualizar();
}

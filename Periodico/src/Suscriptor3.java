/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Suscriptor3 extends Suscriptor{
    
    public Suscriptor3(String nombre){
        this.nombreDeIdentificacion = nombre;
    }

    @Override
    public void actualizar() {
        System.out.println(nombreDeIdentificacion+": Marcando correo como spam...");
    }
   
}

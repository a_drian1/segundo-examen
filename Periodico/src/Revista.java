import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Revista implements Publisher{
    
    private List<Suscriptor> suscriptores;
    
    public Revista(){
        suscriptores = new ArrayList<>();
    }
    
    @Override
    public void agregarSuscriptor(Suscriptor suscriptor){
        suscriptores.add(suscriptor);
    }
    
    @Override
    public void eliminarSuscriptor(Suscriptor suscriptor){
        suscriptores.remove(suscriptor);
    }

    @Override
    public void notificar() {
        suscriptores.forEach((suscriptor) -> {
            System.out.println("Notificando a: "+suscriptor.nombreDeIdentificacion);
            suscriptor.actualizar();
        });
    }
    
}

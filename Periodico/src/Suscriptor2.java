/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Suscriptor2 extends Suscriptor{
    
    public Suscriptor2(String nombre){
        this.nombreDeIdentificacion = nombre;
    }

    @Override
    public void actualizar() {
        System.out.println(nombreDeIdentificacion+": Leyendo correo...");
    }
    
}

/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Suscriptor1 extends Suscriptor{
    
    public Suscriptor1(String nombre){
        this.nombreDeIdentificacion = nombre;
    }

    @Override
    public void actualizar() {
        System.out.println(nombreDeIdentificacion+": Eliminando correo...");
    }

}
